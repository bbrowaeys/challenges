import requests
import os
import polars as pl
import numpy as np
from PIL import Image
from io import BytesIO
from concurrent.futures import ThreadPoolExecutor

URL = 'https://www.pokecardex.com/assets/images/sets/XY/HD/{}'

if os.getenv("IS_BENCHMARKING"):
    URL = "http://localhost:8000/assets/images/sets/XY/HD/{}"


def get_img(url: str):
    img = Image.open(BytesIO(requests.get(url).content), 'r')
    return pl.Series('pixel', np.array(img.getdata()), dtype=pl.List(pl.UInt8))


def main():
    with ThreadPoolExecutor(20) as p:
        res = p.map(get_img, [URL.format(i+1) for i in range(20)])

    res = pl.concat(res).to_frame().lazy().group_by('pixel').agg(pl.count()).top_k(10, by='count').collect()
    print(dict([(tuple(x), y) for x, y in res.rows()]))


if __name__ == '__main__':
    main()